import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';

@NgModule({
  imports: [
    DashboardRoutingModule,
    ChartsModule
  ],
  declarations: [ DashboardComponent ]
  // the view classes that belong to this module.
  // Angular has three kinds of view classes: components, directives, and pipes
  // For this DashboardComponent belongs to this Module
})
export class DashboardModule { }
