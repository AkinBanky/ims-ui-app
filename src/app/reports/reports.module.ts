import {NgModule} from '@angular/core';

import {P404Component} from './404.component';
import {RegisterComponent} from './register.component';
import {ReportsRoutingModule} from './reports-routing.module';
import {CustomerReportComponent} from './customer-report.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatTableModule} from "@angular/material/table";
import {MatSortModule} from "@angular/material/sort";
import {MatIconModule} from "@angular/material/icon";
import {CommonModule} from "@angular/common";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatDialogModule} from "@angular/material/dialog";
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";

@NgModule({
  imports: [ReportsRoutingModule, MatProgressSpinnerModule, MatTableModule, MatSortModule, MatIconModule, CommonModule, MatPaginatorModule, MatDialogModule, ReactiveFormsModule, MatFormFieldModule, MatInputModule],
  declarations: [
    P404Component,
    RegisterComponent,
    CustomerReportComponent
  ]
})
export class ReportsModule { }
