import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {P404Component} from './404.component';
import {RegisterComponent} from './register.component';
import {CustomerReportComponent} from "./customer-report.component";

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Example Pages'
    },
    children: [
      {
        path: '404',
        component: P404Component,
        data: {
          title: 'Page 404'
        }
      },
      {
        path: 'register',
        component: RegisterComponent,
        data: {
          title: 'Register Page'
        }
      },
      {
        path: 'customer',
        component: CustomerReportComponent,
        data: {
          title: 'Customer Report Page'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule {}
