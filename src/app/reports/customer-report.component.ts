import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {CustomerTransactionView} from "../shared/models";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {ReportingService} from "../service/reporting.service";
import {MatDialog} from "@angular/material/dialog";
import {ErrorHandlerService} from "../service/error-handler.service";
import {merge, of as observableOf} from "rxjs";
import {catchError, map, startWith, switchMap} from "rxjs/operators";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-customer',
  templateUrl: './customer-report.component.html',
  styleUrls: ['../../scss/_custom.scss'],
})
export class CustomerReportComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['full name', 'phone', 'amount', 'transaction', 'date'];
  orderPaymentView: CustomerTransactionView[];
  isLoadingResults = true;
  subTotal= 0;
  grandTotal = 0;
  resultsLength = 0;
  private dialogConfig;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  orderPaymentSearchForm: FormGroup;
  fullName: string;

  constructor(private reportService: ReportingService,
              private dialog: MatDialog,
              private formBuilder: FormBuilder,
              private errorHandlerService: ErrorHandlerService) { }

  ngAfterViewInit(): void {
    merge(this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.reportService.getOrderAndPaymentReportForAllUsers(this.paginator.pageIndex);
        }),
        map(data => {
          this.isLoadingResults = false;
          this.resultsLength = data.totalElements;
          this.subTotal = 0;
          // @ts-ignore
          for(let i=0; i< data.numberOfElements; i++){
            // @ts-ignore
            this.subTotal = this.subTotal + data.content[i].orderCost;
          }
          this.grandTotal = data.content[0].grandOrderTotal;
          return data.content;
        }),
        catchError(error => {
          this.errorHandlerService.handleError(error);
          this.isLoadingResults = false;
          return observableOf([]);
        })
      ).subscribe(data => this.orderPaymentView = data);
  }
  ngOnInit() {
    this.orderPaymentSearchForm = this.formBuilder.group({
      userName: [this.fullName, Validators.required]
    });
  }

  viewTransaction(element: any) {

  }

  onSearch(event) {
    let searchText = event.target.value;
    if(searchText === ''){
      this.errorHandlerService.handle400Error("Search text cannot be blank");
      return;
    }

    this.reportService.getOrderAndPaymentReportForUser(searchText)
      .subscribe(res => {
        this.resultsLength = res.totalElements;

        this.subTotal = 0;
        // @ts-ignore
        for(let i=0; i< res.numberOfElements; i++){
          // @ts-ignore
          this.subTotal = this.subTotal + res.content[i].orderCost;
        }

        this.orderPaymentView = res.content;
      });
  }
}
