import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';
import { Router, ActivatedRoute} from '@angular/router';

@Component({
    templateUrl: 'reset-password.component.html'
})
export class ResetPasswordComponent implements OnInit {
    model: any = {};
    message: string;
    token: string;

    constructor(private authenticationService: AuthenticationService,
        private router: Router,
        private route: ActivatedRoute) { }
    ngOnInit() {
        this.model.email = this.route.snapshot.params['email'];
        this.message = '""';
    }

    reset() {
      this.message = '""';
      if (this.model.password !== this.model.password2) {
        this.message = 'Password must match';
        return;
      }
      const token = this.route.snapshot.params['token'];
      this.authenticationService.reset(this.model.email, token, this.model.password)
        .subscribe(
          res => {
            this.router.navigate(['/login']);
          },
          err => {
            if (err.error.errorCode === 404) {
              this.message = 'Token expired';
            } else {
              this.message = err.error.errorMessage;
            }
          });
    }
}
