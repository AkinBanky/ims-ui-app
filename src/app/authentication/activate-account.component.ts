import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthenticationService} from "../service/authentication.service";
import {ErrorHandlerService} from "../service/error-handler.service";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-activate-account',
  templateUrl: './activate-account.component.html',
  styleUrls: ['./activate-account.component.scss']
})
export class ActivateAccountComponent implements OnInit {
  validToken: boolean;
  activateForm: FormGroup;
  submitted = false;
  displayText: any;
  email: string;

  constructor(private formBuilder: FormBuilder,
              private authenticationService: AuthenticationService,
              private errorHandlerService: ErrorHandlerService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    const isValid: string = this.route.snapshot.queryParamMap.get('validToken');
    if (isValid === 'true') {
      this.validToken = true;
      this.displayText = 'Change Password';
      let obj = JSON.parse(localStorage.getItem('currentUser'));
      this.email = obj.emailAddress;
    } else {
      this.validToken = false;
      this.displayText = 'Activate your account';
    }

    this.activateForm = this.formBuilder.group({
      token: ['', Validators.required],
      credential: ['',],
      confirmCredential: ['',]
    });
  }

  get f() {
    return this.activateForm.controls;
  }

  validateToken() {
    this.submitted = true;
    if (this.activateForm.invalid) {
      return;
    }
    this.authenticationService.validateToken(this.f.token.value)
      .subscribe(
        res => {
          this.validToken = true;
          this.displayText = 'Change Password';
          this.email = res.emailAddress;
          //this.router.navigate(['/changePassword']);
        },
        (error: HttpErrorResponse) => {
          this.errorHandlerService.handleError(error);
        });
  }

  changePassword() {

    if (this.f.credential.value === '' || this.f.credential.value === undefined) {
      this.errorHandlerService.handle400Error("Password cannot be empty or null");
      return;
    }

    if (this.f.confirmCredential.value === '' || this.f.confirmCredential.value === undefined) {
      this.errorHandlerService.handle400Error("Reconfirm Password cannot be empty or null");
      return;
    }

    if (this.f.credential.value !== this.f.confirmCredential.value) {
      this.errorHandlerService.handle400Error("Password and Reconfirm password does not match");
      return;
    }
    this.authenticationService.changePassword(this.f.credential.value, this.email)
      .subscribe(
        res => {
          this.router.navigate(['/setup/location']);
        },
        (error: HttpErrorResponse) => {
          this.errorHandlerService.handleError(error);
        });
  }
}
