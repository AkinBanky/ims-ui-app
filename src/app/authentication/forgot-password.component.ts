import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';
import { Response } from '../shared/message';
import { Type } from '../shared/message';

@Component({
    templateUrl: 'forgot-password.component.html'
})
export class ForgotPasswordComponent implements OnInit {
    model: any = {};
    message: string;
    response: Response;

    constructor(private authenticationService: AuthenticationService) { }
    ngOnInit() {
        this.message = '""';
        this.response = new Response()
    }

    forgot() {
        this.message = '""';
        if (!this.model.email) {
            this.response.type = Type.Error;
            this.response.message = 'Please provide email used to setup account';
            return;
        }
        this.authenticationService.forgot(this.model.email)
            .subscribe(
            res => {
              this.response.message = 'Please check your email for reset instructions';
              this.response.type = Type.Success;
            },
            error => {
                this.response.message = error.error;
                this.response.type = Type.Error;
            });
    }
}
