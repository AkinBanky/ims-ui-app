import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../service/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ErrorHandlerService} from '../service/error-handler.service';
import {HttpErrorResponse} from '@angular/common/http';


@Component({
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  model: any = {};
  submitted = false;
  returnUrl: string;
  message: string;
  constructor(private authenticationService: AuthenticationService,
              private route: ActivatedRoute,
              private router: Router,
              private errorHandlerService: ErrorHandlerService,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username:['', Validators.required],
      credential:['', Validators.required],
      remember:['', ]
    });
    this.message = '""';
    this.model.remember = false;
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get f() {return this.loginForm.controls;}

  login() {
    this.submitted = true;
    if(this.loginForm.invalid){
      return;
    }

    this.authenticationService.login(this.f.username.value ,
      this.f.credential.value)
      .subscribe(
        res => {
          this.router.navigate(['/setup/location']);
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
          this.errorHandlerService.handleError(error);
        });
  }

}
