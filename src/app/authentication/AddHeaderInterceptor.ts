import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from "@angular/common/http";
import { Observable } from "rxjs";

const SERVER_PATH = 'ims-web';
export class AddHeaderInterceptor implements HttpInterceptor {
  
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Clone the request to add the new header
    let authorization = localStorage.getItem('authToken');
    const request = JSON.stringify(req);
    if (request.includes(SERVER_PATH) && authorization) {
      const clonedRequest = req.clone({ headers: req.headers.set('Authorization', authorization) });
      return next.handle(clonedRequest);
    } else {
      return next.handle(req);
    }
  }
  }