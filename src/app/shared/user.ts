export interface RootObject {
  entity: Entity[];
  totalSize: number;
}

export interface Entity {
  id: number;
  createdDate: number;
  displayName: string;
  emailAddress: string;
  fullName: string;
  mobileNumber: string;
  locationId: number;
  role: Role;
  createdBy: string;
  status: string;
}

export interface Role {
  id: number;
  createdDate: number;
  displayName: string;
  userRoles: string[];
}
