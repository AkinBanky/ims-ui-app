export class Response {
  message: string;
  type: Type;
}


export enum Type {
  Error,
  Info,
  Success
}
