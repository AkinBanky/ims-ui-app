export interface Location {
    displayName: string;
    address: string;
    status: string;
    id: number;
  }

export interface ProductImage {
  displayName: string;
}

export interface Product {
  displayName: string;
  cartonSize: string;
  wholeQty: number;
  cartonCost: number;
  piecesQty: number;
  location: Location;
  description: string;
  productImages: ProductImage[];
  id: number;
}

export interface ProductDTO {
  productName: string;
  location: string;
  category: string;
}

export interface ReceiptItems {
  override: number;
  pieces: number;
  whole: number;
  productName: string;
  unitCost: number;
  itemCost: number;
}

export interface Transaction {
  displayName: string;
  user: User;
  orderCost: number;
  reversed: boolean;
}

export interface Receipt {
  invoiceNumber: string;
  customerName: string;
  transactionType: string;
  transactionDate: any;
  totalCost: number;
  itemsSet: ReceiptItems[];
}

export interface TransactionDetails {
  wholeQty: number;
  piecesQty: number;
  itemCost: number;
  product: Product;
}

export interface CustomTransactionDetails {
  user: User;
  transactionDetails: TransactionDetails[]
  createdDate: string;
  orderCost: number;
  invoiceNumber: string;
}

export interface Category {
  displayName: string;
  id: number;
}

  export interface LoginResponse {
    emailAddress: string;
    locationManaged: string;
    role: Role;
    userId: number;
  }

  export interface Role {
    description: string;
    userRoles: string[];
    displayName: string;
  }

  export interface User {
    id: number;
    createdDate: number;
    displayName: string;
    emailAddress: string;
    fullName: string;
    mobileNumber: string;
    locationId: number;
    role: Role;
    createdBy: string;
    status: string;
  }

export interface Ledger {
  displayName: string;
  type: string;
  id: number;
}

export interface Payment {
  user: User;
  createdDate: any;
  amount: number;
  description: string;
  id: number;
}

export interface CustomerTransactionView {
  orderCost: number;
  description: string;
  fullName: string;
  transactionType: string;
  ledgerType: string;
  displayName: string;
  createdDate: any;
  mobileNumber: string
  invoiceNumber: string
  grandOrderTotal: number
}
