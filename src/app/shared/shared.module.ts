import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SuccessDialogComponent} from './dialogs/success-dialog.component';
import {ErrorDialogComponent} from './dialogs/error-dialog.component';
import {MatDialogModule} from '@angular/material';
import {ReceiptComponent} from './dialogs/receipt/receipt.component';
import {MatCardModule} from "@angular/material/card";

@NgModule({
  declarations: [
    SuccessDialogComponent,
    ErrorDialogComponent,
    ReceiptComponent,
  ],
  entryComponents :[
    SuccessDialogComponent,
    ErrorDialogComponent,
    ReceiptComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatDialogModule,
    MatCardModule
  ]
})
export class SharedModule { }
