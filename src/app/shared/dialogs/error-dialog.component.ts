import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-error-dialog',
  templateUrl: './error-dialog.component.html',
  styleUrls: ['./error-dialog.component.scss']
})
export class ErrorDialogComponent implements OnInit {
  public error: string;
  constructor(private dialogRef: MatDialogRef<ErrorDialogComponent>,@Optional() @Inject(MAT_DIALOG_DATA) private data: any) { }

  ngOnInit() {
    this.error = this.data;
  }

  closeDialog(){
    this.dialogRef.close();
  }

}
