import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import * as  jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';
import {ReceiptItems} from "../../models";

@Component({
  selector: 'app-receipt',
  templateUrl: './receipt.component.html',
  styleUrls: ['./receipt.component.scss']
})
export class ReceiptComponent implements OnInit {

  private dialogConfig;
  constructor(private dialogRef: MatDialogRef<ReceiptComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private dialog: MatDialog) { }

invoiceNumber: string;
transactionType: string;
totalCost: number;
customerName: string;
transactionDate: string;
itemsSet: ReceiptItems[] = [];
description: string;
isTransactionOrder: boolean;

  ngOnInit() {
    this.dialogConfig = {
      disableClose: true,
      autoFocus: true,
      width: '650px'
    };
  this.invoiceNumber = this.data.invoiceNumber;
  this.transactionType = this.data.transactionType;
  this.totalCost = this.data.totalCost;
  this.customerName = this.data.customerName;
  this.itemsSet = this.data.itemsSet;
  this.description = this.data.description;
  this.transactionDate = this.data.transactionDate;
  this.isTransactionOrder = !(this.data.itemsSet === null || this.data.itemsSet === undefined);
  }

  download() {
    const div = document.getElementById("invoice");
    const options = {background: "white", height: div.clientHeight, width: div.clientWidth};

    // @ts-ignore
    html2canvas(div, options).then((canvas) => {
      //Initialize JSPDF
      let doc = new jsPDF("p", "mm", "a4");
      doc.setFont("arial", "bold");
      //Converting canvas to Image
      let imgData = canvas.toDataURL("image/PNG");
      //Add image Canvas to PDF
      doc.addImage(imgData, 'PNG', 20, 20);

      let pdfOutput = doc.output();
      // using ArrayBuffer will allow you to put image inside PDF
      let buffer = new ArrayBuffer(pdfOutput.length);
      let array = new Uint8Array(buffer);
      for (let i = 0; i < pdfOutput.length; i++) {
        array[i] = pdfOutput.charCodeAt(i);
      }
      //Name of pdf
      const fileName = this.customerName + ".pdf";
      // Make file
      doc.save(fileName);
    });
  }
}
