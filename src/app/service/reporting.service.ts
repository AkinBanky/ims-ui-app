import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {CustomerTransactionView} from "../shared/models";
import {Observable} from "rxjs";
import {URLSettings} from "../shared/URLSettings";

@Injectable({
  providedIn: 'root'
})
export class ReportingService {

  constructor(private http: HttpClient) { }

  getOrderAndPaymentReportForAllUsers(page: number): Observable<CustomerTransactionViewResponseApi> {
    const href = URLSettings.API_ENDPOINT + '/reports/users';
    const requestUrl = `${href}?page=${page + 1}`;
    return this.http.get<CustomerTransactionViewResponseApi>(requestUrl);
  }

  getOrderAndPaymentReportForUser(userName: string): Observable<CustomerTransactionViewResponseApi> {
    const href = URLSettings.API_ENDPOINT + '/reports/users/' + userName;
    const requestUrl = `${href}`;
    return this.http.get<CustomerTransactionViewResponseApi>(requestUrl);
  }
}

export interface CustomerTransactionViewResponseApi {
  content: CustomerTransactionView[];
  totalElements: number;
}
