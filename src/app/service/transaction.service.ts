import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {ErrorHandlerService} from "./error-handler.service";
import {Receipt} from "../shared/models";
import {URLSettings} from "../shared/URLSettings";
import {catchError, tap} from "rxjs/operators";
import {Observable, of as observableOf} from "rxjs";
import {SortDirection} from "@angular/material/sort";

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(private http: HttpClient,
              private errorHandlerService: ErrorHandlerService) {
  }

  createTransaction(itemCart: Array<{
                      productName: string; whole: number; pieces: number;
                      productId: number; transactionType: string; offerType: string; offerValue: number, ledger: string
                    }>,
                    userId: number) {

    const body = {
      userId: userId,
      channel: 'ADMIN',
      transactionDetailsDtoSet: itemCart
    };
    return this.http
      .post<Receipt>(URLSettings.API_ENDPOINT + '/transactions', body)
      .pipe(
        tap(usr => console.log('Transaction successful ')),
        catchError(error => {
          this.errorHandlerService.handleError(error);
          return observableOf([]);
        })
      );
  }

  retrieveTransactions(order: SortDirection, page: number): Observable<TransactionResponseApi> {
    const href = URLSettings.API_ENDPOINT + '/transactions';
    const requestUrl =
      `${href}?page=${page + 1}&order=${order}`;
    return this.http.get<TransactionResponseApi>(requestUrl);
  }

  getTransactionBySearchText(searchText: string): Observable<TransactionResponseApi> {
    const href = URLSettings.API_ENDPOINT + '/transactions';
    const requestUrl = `${href}/${searchText}`;
    return this.http.get<TransactionResponseApi>(requestUrl);
  }

  reverseTransaction(invoiceNumber: string){
    const body = {
      invoiceNumber: invoiceNumber
    };
    let search = new HttpParams();
    search.set("invoiceNumber", invoiceNumber);
    return this.http
      .post<Receipt>(URLSettings.API_ENDPOINT + '/transactions/reverse', body)
      .pipe(
        tap(usr => console.log('Transaction reversed successfully ')),
        catchError(error => {
          this.errorHandlerService.handleError(error);
          return observableOf([]);
        })
      );
  }

  reverseSelectedItems(reverseCart: Array<{
    productName: string; whole: number; pieces: number; itemCost: number; productId: number;
  }>, invoiceNumber: string) {
    const body = {
      invoiceNumber: invoiceNumber,
      transactionDetailsDtoSet: reverseCart
    };
    return this.http
      .post<Receipt>(URLSettings.API_ENDPOINT + '/transactions/reverse/items', body)
      .pipe(
        tap(usr => console.log('Reversal successful ')),
        catchError(error => {
          this.errorHandlerService.handleError(error);
          return observableOf([]);
        })
      );
  }
}

export interface TransactionResponseApi {
  content: Receipt[];
  totalElements: number;
}
