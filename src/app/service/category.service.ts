import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ErrorHandlerService} from "./error-handler.service";
import {Category, Location} from "../shared/models";
import {URLSettings} from "../shared/URLSettings";
import {Observable, of as observableOf} from "rxjs";
import {catchError, tap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(public http: HttpClient,
              private errorHandlerService: ErrorHandlerService) {
  }

  /**
   * Returns paginated location object
   * @param page
   */
  getAllCategoriesWithPagination(order: string, page: number): Observable<CategoryResponseApi> {
    const href = URLSettings.API_ENDPOINT + '/categories';
    const requestUrl =
      `${href}?page=${page + 1}&order=${order}`;
    return this.http.get<CategoryResponseApi>(requestUrl);
  }

  getAllCategories(): Observable<Category[]> {
    const requestUrl = URLSettings.API_ENDPOINT + '/categories/all';
    return this.http.get<Category[]>(requestUrl);
  }

  createCategory(categoryName: string) {
    const body = {
      categoryName: categoryName
    };

    return this.http
      .post<Location>(URLSettings.API_ENDPOINT + '/categories', body)
      .pipe(
        tap(cat => console.log('Created new category')),
        catchError(error => {
          this.errorHandlerService.handleError(error);
          return observableOf([]);
        })
      );
  }

  updateCategory(categoryName: string, categoryId: number) {
    const body = {
      categoryName: categoryName
    };
    return this.http
      .put<Location>(URLSettings.API_ENDPOINT + '/categories/' + categoryId, body)
      .pipe(
        tap(cat => console.log('Updated category')),
        catchError(error => {
          this.errorHandlerService.handleError(error);
          return observableOf([]);
        })
      );
  }
}


export interface CategoryResponseApi {
  content: Category[];
  totalElements: number;
}
