import {Injectable, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {ErrorDialogComponent} from '../shared/dialogs/error-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService implements OnInit {
  private dialogConf;
  private errorMessage: string = '';

  constructor(private dialog: MatDialog,
    private router: Router) { }

  ngOnInit() {
    this.dialogConf = {
      disableClose: false,
      autoFocus: true,
      width: '450px'
    };
  }

  public handle400Error(message: string){
    let error = new HttpErrorResponse({
      status: 400,
      error: {
        errorMessage: message
      },
      statusText: message
    });
    this.dialogConf = {
      disableClose: false,
      autoFocus: true,
      width: '450px'
    };
    this.createErrorMessage(error);
    this.dialogConf.data = this.errorMessage;
    this.dialog.open(ErrorDialogComponent, this.dialogConf);
  }
  public handleError(error: HttpErrorResponse) {

    switch (error.status) {
      case 500:
        this.router.navigate(['/reports/404']);
        break;
      case 401:
        this.dialogConf = {
          disableClose: false,
          autoFocus: true,
          width: '450px'
        };
        this.createErrorMessage(error);
        this.dialogConf.data = this.errorMessage;
        this.dialog.open(ErrorDialogComponent, this.dialogConf);
        this.router.navigate(['/login']);
        break;
      default:
        this.dialogConf = {
          disableClose: false,
          autoFocus: true,
          width: '450px'
        };
        this.createErrorMessage(error);
        this.dialogConf.data = this.errorMessage;
        this.dialog.open(ErrorDialogComponent, this.dialogConf);
    }
  }

  private createErrorMessage(error: HttpErrorResponse) {
    this.errorMessage = error.message ? error.error.errorMessage : JSON.stringify(error);
  }
}
