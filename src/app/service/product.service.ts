import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ErrorHandlerService} from "./error-handler.service";
import {Observable} from "rxjs";
import {URLSettings} from "../shared/URLSettings";
import {Product, ProductDTO} from "../shared/models";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient,
              private errorHandlerService: ErrorHandlerService) {
  }

  getAllProducts(order: string, sortColumn: string, page: number): Observable<ProductResponseApi> {
    const href = URLSettings.API_ENDPOINT + '/products';
    const requestUrl =
      `${href}?page=${page + 1}&sortColumn=${sortColumn}&order=${order}`;
    return this.http.get<ProductResponseApi>(requestUrl);
  }

  createProduct(productName: string, piecesQty: number,
                wholeQty: number, cartonCost: number,
                location: string, category: string, data: FormData, size: number, description: string) {
    const body = {
      productName: productName,
      piecesQty: piecesQty,
      wholeQty: wholeQty,
      cartonCost: cartonCost,
      location: location,
      category: category,
      cartonSize: size,
      description: description
    };
    data.append("model", JSON.stringify(body));

    return this.http
      .post<ProductDTO>(URLSettings.API_ENDPOINT + '/products', data);
  }

  getProductsBySearchText(searchText: string): Observable<ProductResponseApi> {
    const href = URLSettings.API_ENDPOINT + '/products';
    const requestUrl = `${href}/${searchText}`;
    return this.http.get<ProductResponseApi>(requestUrl);
  }

  updateProduct(productName: string, piecesQty: number,
                wholeQty: number, cartonCost: number,
                location: string, category: string,
                data: FormData, size: number, productId: number, description: string) {
    const updateBody = {
      productName: productName,
      piecesQty: piecesQty,
      wholeQty: wholeQty,
      cartonCost: cartonCost,
      location: location,
      category: category,
      cartonSize: size,
      description: description
    };
    data.append("model", JSON.stringify(updateBody));
    return this.http
      .put<ProductDTO>(URLSettings.API_ENDPOINT + '/products/' + productId, data);
  }

  getAllProductsInLocation(value: number): Observable<Product[]> {
    const requestUrl = URLSettings.API_ENDPOINT + '/products/locations/' + value + '/all';
    return this.http.get<Product[]>(requestUrl);
  }
}


export interface ProductResponseApi {
  content: Product[];
  totalElements: number;
}
