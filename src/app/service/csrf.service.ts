import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class CsrfService {

  constructor(private cookieService: CookieService) { }

  getCSRF(name?: string) {
    name = `${name ? name : 'X-XSRF-TOKEN'}`;
    console.log('This is the csrf name ' + name);
    return this.cookieService.get(name);
  }
}
