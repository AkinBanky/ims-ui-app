import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map'; // for observable
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {User} from '../shared/models';
import {URLSettings} from '../shared/URLSettings';
import {catchError, tap} from 'rxjs/operators';
import {of as observableOf} from 'rxjs';
import {ErrorHandlerService} from './error-handler.service';


@Injectable()
export class UserService {
  constructor(public http: HttpClient,
    private errorHandlerService: ErrorHandlerService) { }

  retrievePaginatedUsers(order: string, page: number): Observable<UserResponseApi> {
    const href = URLSettings.API_ENDPOINT + '/users';
    const requestUrl =
      `${href}?page=${page + 1}&order=${order}`;
    return this.http.get<UserResponseApi>(requestUrl);
  }

  getAllUsers(): Observable<User[]> {
    const requestUrl = URLSettings.API_ENDPOINT + '/users/all';
    return this.http.get<User[]>(requestUrl);
  }


  createUser(username: string, fullname: string,
    mobileNumber: string, userRoles: string[],
    emailAddress: string) {
    const body = {
      username: username,
      fullName: fullname,
      emailAddress: emailAddress,
      mobileNumber: mobileNumber,
      roles: userRoles
    };

    return this.http
      .post<User>(URLSettings.API_ENDPOINT + '/users', body)
      .pipe(
        tap(usr => console.log('Created new user ')),
        catchError(error => {
          this.errorHandlerService.handleError(error);
          return observableOf([]);
        })
      );
  }

  updateUser(username: string, fullname: string,
    mobileNumber: string, userRoles: string[], emailAddress: string, userId: number) {
      const body = {
        username: username,
        fullName: fullname,
        emailAddress: emailAddress,
        mobileNumber: mobileNumber,
        roles: userRoles
      };
    return this.http
      .put<User>(URLSettings.API_ENDPOINT + '/users/' + userId, body)
      .pipe(
        tap(() => console.log('Updated user ')),
        catchError(error => {
          this.errorHandlerService.handleError(error);
          return observableOf([]);
        })
      );
  }

  activateOrDeactivateStatus(status: string, userId: number){
    const body ={
      status: status
    };
    return this.http
        .put<any>(URLSettings.API_ENDPOINT + '/users/ +' + userId + '/status', body)
        .pipe(
          catchError(error => {
            this.errorHandlerService.handleError(error);
            return observableOf([]);
          })
        )
  }
}

export interface UserResponseApi {
  content: User[];
  totalElements: number;
}
