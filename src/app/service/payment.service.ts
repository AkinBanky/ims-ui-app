import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ErrorHandlerService} from "./error-handler.service";
import {Payment, Receipt} from "../shared/models";
import {Observable, of as observableOf} from "rxjs";
import {URLSettings} from "../shared/URLSettings";
import {catchError, tap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(private http: HttpClient,
              private errorHandlerService: ErrorHandlerService) { }

  retrievePayments(order: string, page: number): Observable<ReceiptResponseApi>  {
    let sortColumn = 'date';
    const href = URLSettings.API_ENDPOINT + '/payments';
    const requestUrl =
      `${href}?page=${page + 1}&order=${order}&sortColumn=${sortColumn}`;
    return this.http.get<ReceiptResponseApi>(requestUrl);
  }


  makePayment(userId: number, ledgerId: number, amount: number, narration: string) {
    const body = {
      userId: userId,
      ledgerId: ledgerId,
      amount: amount,
      narration: narration
    };
    return this.http
      .post<Receipt>(URLSettings.API_ENDPOINT + '/payments', body)
      .pipe(
        tap(usr => console.log('payment made for User')),
        catchError(error => {
          this.errorHandlerService.handleError(error);
          return observableOf([]);
        })
      );
  }
}

export interface PaymentResponseApi {
  content: Payment[];
  totalElements: number;
}

export interface ReceiptResponseApi {
  content: Receipt[];
  totalElements: number;
}
