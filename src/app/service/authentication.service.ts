import {Injectable} from '@angular/core';

import 'rxjs/add/operator/map'; // for observable
import {HttpClient} from '@angular/common/http';
import {URLSettings} from '../shared/URLSettings';
import {LoginResponse} from '../shared/models';

//import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class AuthenticationService {

  logout() {
    localStorage.clear();
  }

  constructor(public http: HttpClient) { }

  login(username: string, credential: string) {
    const body = {
      username: username,
      credential: credential
    };

    return this.http
      .post<LoginResponse>(URLSettings.API_ENDPOINT + '/cjgrocery/authenticate', body, {
        withCredentials: true,
        observe: 'response',
      }).map(user => {
        if (user && user.body.userId) {
          localStorage.setItem('authToken', user.headers.get('Authorization'));
          localStorage.setItem('currentUser', JSON.stringify(user.body));
        }
        return user;
      });
  }

  private extractInfoFromToken(newToken: string) {
    const base64HeaderUrl = newToken.split('.')[0];
    const base64Header = base64HeaderUrl.replace('-', '+').replace('_', '/');
    const headerData = JSON.parse(window.atob(base64Header));
    //console.log(headerData);
    //console.log(headerData.tenant_id);
  }

  forgot(email: string) {
    const body = {
      email: email
    };
    return this.http
      .post<any>(URLSettings.API_ENDPOINT + '/cjgrocery/users/forgotPassword/', body, { withCredentials: true })
      .map(
        result => {
          return result;
        });
  }

  validateToken(token: string) {
    const body = {
      token: token
    };
    return this.http
      .post<any>(URLSettings.API_ENDPOINT + '/cjgrocery/tokenizer/validate/', body, { withCredentials: true })
      .map(
        result => {
          return result;
        }
      );
  }

  reset(email: string, token: string, password: string) {
    const body = {
      emailAddress: email,
      token: token,
      password: password
    };
    return this.http
      .put(URLSettings.API_ENDPOINT + '/cjgrocery/password/reset/', body, { withCredentials: true })
      .map(
        result => {
          return result;
        }
      );
  }

  changePassword(credential: string, emailAddress: string) {
    const body = {
      password: credential,
      emailAddress: emailAddress
    };
    return this.http
      .put<any>(URLSettings.API_ENDPOINT + '/cjgrocery/password/change', body, { withCredentials: true })
      .map(
        () => {
          localStorage.clear();
        });
  }
}

