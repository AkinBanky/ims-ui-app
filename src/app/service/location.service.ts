import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of as observableOf} from 'rxjs';
import {Location} from '../shared/models';
import {URLSettings} from '../shared/URLSettings'
import {catchError, tap} from 'rxjs/operators';
import {ErrorHandlerService} from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private http: HttpClient,
              private errorHandlerService: ErrorHandlerService) {
  }
  /**
   * Returns paginated location object
   * @param page
   */
  getAllLocationsWithPagination(order: string, page: number): Observable<LocationResponseApi> {
    const href = URLSettings.API_ENDPOINT + '/locations';
    const requestUrl =
      `${href}?page=${page + 1}&order=${order}`;
    return this.http.get<LocationResponseApi>(requestUrl);
  }

  getAllLocations(): Observable<Location[]> {
    const requestUrl = URLSettings.API_ENDPOINT + '/locations/all';
    return this.http.get<Location[]>(requestUrl);
  }


  createLocation(locationName: string, locationAddress: string) {
    const body = {
      name: locationName,
      address: locationAddress
    };


    return this.http
      .post<Location>(URLSettings.API_ENDPOINT + '/locations', body)
      .pipe(
        tap(loc => console.log('Created new location ')),
        catchError(error => {
          this.errorHandlerService.handleError(error);
          return observableOf([]);
        })
      );
  }

  updateLocation(locationName: string, locationAddress: string, locationId: number) {
    const body = {
      name: locationName,
      address: locationAddress
    };
    return this.http
      .put<Location>(URLSettings.API_ENDPOINT + '/locations/' + locationId, body)
      .pipe(
        tap(loc => console.log('Updated location ')),
        catchError(error => {
          this.errorHandlerService.handleError(error);
          return observableOf([]);
        })
      );
  }
}

export interface LocationResponseApi {
  content: Location[];
  totalElements: number;
}
