import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ErrorHandlerService} from "./error-handler.service";
import {Observable} from "rxjs";
import {URLSettings} from "../shared/URLSettings";
import {Ledger} from "../shared/models";

@Injectable({
  providedIn: 'root'
})
export class LedgerService {

  constructor(private http: HttpClient,
              private errorHandler: ErrorHandlerService) { }

  getAllLedgers(): Observable<Ledger[]> {
    const requestUrl = URLSettings.API_ENDPOINT + '/ledgers';
    return this.http.get<Ledger[]>(requestUrl);
  }
}
