import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SimpleLineIconsComponent} from './simple-line-icons.component';
import {CustomerOrderComponent} from "./customer-order/customer-order.component";
import {CustomerPaymentComponent} from "./customer-payments/customer-payment.component";
import {OrderHistoryComponent} from "./order-history/order-history.component";

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Transaction' // what goes on the breadcrumb
    },
    children: [
      {
        path: 'order',
        component: CustomerOrderComponent,
        data: {
          title: 'Customer Order'
        }
      },
      {
        path: 'order-history',
        component: OrderHistoryComponent,
        data: {
          title: 'Order History'
        }
      },

      {
        path: 'payments',
        component: CustomerPaymentComponent,
        data: {
          title: 'Customer Payments'
        }
      },
      {
        path: 'simple-line-icons',
        component: SimpleLineIconsComponent,
        data: {
          title: 'Simple Line Icons'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionsRoutingModule {}
