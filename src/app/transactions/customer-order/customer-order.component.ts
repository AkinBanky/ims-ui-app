import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LocationService} from "../../service/location.service";
import {Ledger, Location, Product, User} from "../../shared/models";
import {UserService} from "../../service/user.service";
import {ProductService} from "../../service/product.service";
import {TransactionService} from "../../service/transaction.service";
import {MatDialog} from "@angular/material";
import {ErrorHandlerService} from "../../service/error-handler.service";
import {LedgerService} from "../../service/ledger.service";
import {ReceiptComponent} from "../../shared/dialogs/receipt/receipt.component";

type itemList = Array<{
  productName: string,
  whole: number,
  pieces: number,
  productId: number,
  transactionType: string,
  offerType: string,
  offerValue: number,
  ledger: string,
}>;


@Component({
  selector: 'app-requisition-note',
  templateUrl: './customer-order.component.html',
  styleUrls: ['./customer-order.component.scss']
})


export class CustomerOrderComponent implements OnInit {
  requisitionForm: FormGroup;
  hideElement: boolean;
  itemCart: itemList = [];
  transactionType: string[] = [];
  locationList: Location[] = [];
  ledgerList: Ledger[] = [];
  userList: User[] = [];
  productList: Product[] = [];
  location: number;
  customer: number;
  carton: number;
  pieces: number;
  product: number;
  offerValue: number;
  offerType: string;
  stockWhole: number;
  stockPieces: number;
  stockCost: number;
  type: string;
  ledger: string;
  userName: string;
  private dialogConfig;

  constructor(private formBuilder: FormBuilder,
              private locationService: LocationService,
              private userService: UserService,
              private productService: ProductService,
              private ledgerService: LedgerService,
              private dialog: MatDialog,
              private transactionService: TransactionService,
              private errorHandlerService: ErrorHandlerService) {
  }

  ngOnInit() {
    this.hideElement = false;

    this.userService.getAllUsers()
      .subscribe(res => {
        this.userList = res;
      });

    this.locationService.getAllLocations()
      .subscribe(res => {
        this.locationList = res;
      });

    this.ledgerService.getAllLedgers()
      .subscribe(res => {
        this.ledgerList = res;
      });

    this.dialogConfig = {
      disableClose: false,
      autoFocus: true,
      width: '450px'
    };

    this.requisitionForm = this.formBuilder.group({
      customer: [this.customer, Validators.required],
      location: [this.location, Validators.required],
      carton: [this.carton, Validators.required],
      pieces: [this.pieces, ''],
      product: [this.product, Validators.required],
      offerType: [this.offerType, ''],
      offerValue: [this.offerValue, ''],
      type: [this.type, Validators.required],
    });
  }

  get f() {
    return this.requisitionForm.controls;
  }

  isModified(value: any) {
    this.hideElement = value !== undefined;
  }

  addToList() {
    if(this.transactionType.length == 0){
      this.transactionType.push(this.f.type.value);
    } else {
      if(this.transactionType.includes(this.f.type.value) ==false ){
        this.errorHandlerService.handle400Error("Only one Transaction Type allowed per Invoice");
        return;
      }
    }
    this.itemCart.push({
      productName: this.f.product.value.displayName,
      pieces: this.f.pieces.value === null ? 0: this.f.pieces.value,
      whole: this.f.carton.value,
      productId: this.f.product.value.id,
      transactionType: this.f.type.value,
      offerType: this.f.offerType.value,
      offerValue: this.f.offerValue.value,
      ledger: this.f.type.value === 'cash_invoice' ? 'Cash invoice transaction' : "N/A"
    });
    this.userName = this.f.customer.value.displayName;
  }

  loadProduct(value: number) {
    this.stockWhole = null;
    this.stockPieces = null;
    this.stockCost = null;
    this.productService.getAllProductsInLocation(value)
      .subscribe(res => {
        this.productList = res;
      });
  }

  retrieveProductDetails(value: any) {
    this.stockWhole = value.wholeQty;
    this.stockPieces = value.piecesQty;
    this.stockCost = value.cartonCost;
  }

  removeFromList(productName: string) {
    let i = this.itemCart.length;
    while (i--) {
      if (this.itemCart[i].productName === productName) {
        this.itemCart.splice(i, 1);
      }
    }
  }

  createTransaction(itemCart: Array<{ productName: string; whole: number; pieces: number; productId: number;
  transactionType: string; offerType: string; offerValue: number; ledger: string }>) {
    let userId = this.f.customer.value.id;
    this.transactionService.createTransaction(itemCart, userId)
      .subscribe((result: any) => {
        if (result.length !== 0) {
          this.dialogConfig.data = result; //pass the data to the receipt module
          this.dialog.open(ReceiptComponent, this.dialogConfig);
        }
      });
    this.itemCart = []; //clear the array
    this.transactionType = [];

  }
}
