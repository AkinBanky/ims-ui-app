import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from "@angular/material/dialog";
import {CustomTransactionDetails} from "../../shared/models";
import {ReceiptComponent} from "../../shared/dialogs/receipt/receipt.component";
import {TransactionService} from "../../service/transaction.service";

type reverseList = Array<{
  productName: string,
  whole: number,
  pieces: number,
  itemCost: number,
  productId: number,
}>;

@Component({
  selector: 'app-order-history-details',
  templateUrl: './order-history-details.component.html',
  styleUrls: ['../../../scss/_custom.scss']
})
export class OrderHistoryDetailsComponent implements OnInit {
  displayedColumns: string[] = ['product', 'whole', 'pieces', 'cost', 'adjustment', 'reverse'];
  transaction: CustomTransactionDetails;
  reverseCart: reverseList = [];
  private dialogConfig;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private transactionService: TransactionService,
              private dialog: MatDialog,) { }

  ngOnInit() {
    this.transaction = this.data;
    this.dialogConfig = {
      disableClose: false,
      autoFocus: true,
      width: '750px'
    };
  }

  reverseTransaction(element: any) {
    this.reverseCart.push({
      productName: element.product.displayName,
      pieces: element.piecesQty,
      whole: element.wholeQty,
      itemCost: element.itemCost,
      productId: element.product.id
    });
  }

  removeFromList(productName: string) {
    let i = this.reverseCart.length;
    while (i--) {
      if (this.reverseCart[i].productName === productName) {
        this.reverseCart.splice(i, 1);
      }
    }
  }

  reverseSelectedItems(reverseCart: reverseList) {
    this.transactionService.reverseSelectedItems(reverseCart, this.data.invoiceNumber)
      .subscribe(res => {
        this.dialogConfig.data = res; //pass the data to the receipt module
        if (Array.isArray(res) && res.length === 0 ) {
          return;
        }
        this.dialog.open(ReceiptComponent, this.dialogConfig);
      });
  }
}
