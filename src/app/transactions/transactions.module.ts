import {NgModule} from '@angular/core';

import {FontAwesomeComponent} from './font-awesome.component';
import {SimpleLineIconsComponent} from './simple-line-icons.component';

import {TransactionsRoutingModule} from './transactions-routing.module';
import {CustomerOrderComponent} from './customer-order/customer-order.component';
import {CreditNoteComponent} from './credit-note/credit-note.component';
import {CustomerPaymentComponent} from './customer-payments/customer-payment.component';
import {
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule
} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {PaymentModalComponent} from './customer-payments/payment-modal/payment-modal.component';
import {OrderHistoryComponent} from './order-history/order-history.component';
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {OrderHistoryDetailsComponent} from './order-history-details/order-history-details.component';

@NgModule({
  imports:
    [TransactionsRoutingModule,
      MatDialogModule,
      MatFormFieldModule,
      MatSelectModule,
      CommonModule,
      FormsModule,
      MatInputModule,
      ReactiveFormsModule, MatProgressSpinnerModule, MatTableModule, MatPaginatorModule, MatSortModule, MatButtonModule, MatIconModule],
  declarations: [
    FontAwesomeComponent,
    SimpleLineIconsComponent,
    CustomerOrderComponent,
    CreditNoteComponent,
    CustomerPaymentComponent,
    PaymentModalComponent,
    OrderHistoryComponent,
    OrderHistoryDetailsComponent
  ],
  entryComponents :[PaymentModalComponent, OrderHistoryDetailsComponent]
})
export class TransactionsModule { }
