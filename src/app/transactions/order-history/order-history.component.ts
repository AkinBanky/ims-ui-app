import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Receipt} from "../../shared/models";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatDialog} from "@angular/material/dialog";
import {ErrorHandlerService} from "../../service/error-handler.service";
import {TransactionService} from "../../service/transaction.service";
import {merge, of as observableOf} from "rxjs";
import {catchError, map, startWith, switchMap} from "rxjs/operators";
import {OrderHistoryDetailsComponent} from "../order-history-details/order-history-details.component";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ReceiptComponent} from "../../shared/dialogs/receipt/receipt.component";

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['../../../scss/_custom.scss'],
})
export class OrderHistoryComponent implements AfterViewInit, OnInit {
  displayedColumns: string[] = ['username', 'full name', 'date', 'transaction', 'cost', 'view', 'reverse'];
  transaction: Receipt[];
  transactionSearchForm: FormGroup;
  userName: string;
  resultsLength = 0;
  isLoadingResults = true;
  private dialogConfig;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private transactionService: TransactionService,
              private dialog: MatDialog,
              private formBuilder: FormBuilder,
              private errorHandlerService: ErrorHandlerService) {}
  ngAfterViewInit(): void {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.transactionService.retrieveTransactions(this.sort.direction, this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.resultsLength = data.totalElements;
          return data.content;
        }),
        catchError(error => {
          this.errorHandlerService.handleError(error);
          this.isLoadingResults = false;
          return observableOf([]);
        })
      ).subscribe(data => this.transaction = data);
  }

  private initializeModal() {
    this.dialogConfig = {
      disableClose: false,
      autoFocus: true,
      width: '750px'
    };
  }

  ngOnInit(): void {
    this.initializeModal();
    this.transactionSearchForm = this.formBuilder.group({
      userName: [this.userName, Validators.required]
    });
  }


  viewTransaction(element: any) {
    this.dialogConfig.data = element;
    this.dialog.open(OrderHistoryDetailsComponent, this.dialogConfig);
  }

  onSearch(event) {
    let searchText = event.target.value;
    if(searchText === ''){
      this.errorHandlerService.handle400Error("Search text cannot be blank");
      return;
    }

    this.transactionService.getTransactionBySearchText(searchText)
      .subscribe(res => {
        this.resultsLength = res.totalElements;
        this.transaction = res.content;
      });
  }

  reverseTransaction(element: any) {
    //reverse the entire transaction
    if (element.transactionType === 'RETURNS') {
      this.errorHandlerService.handle400Error("Returned Transaction cannot be reversed");
      return;
    }
    this.transactionService.reverseTransaction(element.invoiceNumber)
      .subscribe(res => {
        this.dialogConfig.data = res; //pass the data to the receipt module
        if (Array.isArray(res) && res.length === 0 ) {
          return;
        }
        this.dialog.open(ReceiptComponent, this.dialogConfig);
      });
  }
}
