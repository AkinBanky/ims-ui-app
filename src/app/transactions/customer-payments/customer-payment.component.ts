import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Receipt} from "../../shared/models";
import {MatDialog, MatPaginator, MatSort} from "@angular/material";
import {ErrorHandlerService} from "../../service/error-handler.service";
import {merge, of as observableOf} from "rxjs";
import {catchError, map, startWith, switchMap} from "rxjs/operators";
import {PaymentService} from "../../service/payment.service";
import {PaymentModalComponent} from "./payment-modal/payment-modal.component";

@Component({
  selector: 'app-credit-invoice',
  templateUrl: './customer-payment.component.html',
  styleUrls: ['./customer-payment.component.scss']
})
export class CustomerPaymentComponent implements AfterViewInit, OnInit {
  displayedColumns: string[] = ['fullname', 'description', 'amount', 'date'];
  payment: Receipt[] = [];
  resultsLength = 0;
  isLoadingResults = true;
  private dialogConfig;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private dialog: MatDialog,
              private errorHandlerService: ErrorHandlerService,
              private paymentService: PaymentService) { }

  ngOnInit() {
    this.initializeModal();
  }

  private initializeModal() {
    this.dialogConfig = {
      disableClose: false,
      autoFocus: true,
      width: '450px'
    };
  }

  ngAfterViewInit(): void {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.paymentService.retrievePayments(this.sort.direction, this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.resultsLength = data.totalElements;
          return data.content;
        }),
        catchError(error => {
          this.errorHandlerService.handleError(error);
          this.isLoadingResults = false;
          return observableOf([]);
        })
      ).subscribe(data => this.payment = data);
  }

  makePaymentDialog(){
    this.initializeModal();
    this.dialog.open(PaymentModalComponent, this.dialogConfig);
  }

}
