import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material";
import {PaymentService} from "../../../service/payment.service";
import {UserService} from "../../../service/user.service";
import {LedgerService} from "../../../service/ledger.service";
import {Ledger, User} from "../../../shared/models";
import {ReceiptComponent} from "../../../shared/dialogs/receipt/receipt.component";

@Component({
  selector: 'app-payment-modal',
  templateUrl: './payment-modal.component.html'
})
export class PaymentModalComponent implements OnInit {
  paymentForm: FormGroup;
  private dialogConfig;
  userList: User[] = [];
  ledgerList: Ledger[] = [];
  amount: number;
  user: string;
  narration: string;
  ledger: string;
  constructor(private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<PaymentModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private dialog: MatDialog,
              private paymentService: PaymentService,
              private userService: UserService,
              private ledgerService: LedgerService) { }

  ngOnInit() {

    this.dialogConfig = {
      disableClose: false,
      autoFocus: true,
      width: '450px'
    };

    this.userService.getAllUsers()
      .subscribe(res => {
        this.userList = res;
      });

    this.ledgerService.getAllLedgers()
      .subscribe(res => {
        this.ledgerList = res;
      });
    this.amount = 0;

    this.paymentForm = this.formBuilder.group({
      user: [this.user, Validators.required],
      ledger: [this.ledger, Validators.required],
      amount: [this.amount, Validators.required],
      narration: [this.narration, ''],
    });
  }

  get f() {
    return this.paymentForm.controls;
  }

  save() {
    this.paymentService.makePayment(this.f.user.value, this.f.ledger.value,
      this.f.amount.value, this.f.narration.value)
      .subscribe((result: any) => {
        if (result.length !== 0) {
          this.dialogConfig.data = result; //pass the data to the receipt module
          this.dialogRef.close(this.paymentForm.value);
          this.dialog.open(ReceiptComponent, this.dialogConfig);
        }
      });
  }

  close() {
    this.dialogRef.close();
  }
}
