import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {merge, of as observableOf} from 'rxjs';
import {LocationModalComponent} from './location-modal/location-modal.component';
import {Location} from '../shared/models';
import {LocationService} from '../service/location.service';
import {ErrorHandlerService} from '../service/error-handler.service';

@Component({
  templateUrl: './locations.component.html',
  styleUrls: ['./setup.component.scss']
})
export class LocationsComponent implements AfterViewInit, OnInit {
  displayedColumns: string[] = ['location', 'address', 'status', 'update'];
  data: Location[] = [];
  resultsLength = 0;
  isLoadingResults = true;
  private dialogConfig;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit(){
    this.initializeModal();
  }

  constructor(private dialog: MatDialog,
              private locationService: LocationService,
              private errorHandlerService: ErrorHandlerService) { }

  private initializeModal() {
    this.dialogConfig = {
      disableClose: false,
      autoFocus: true,
      width: '450px'
    };
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.locationService.getAllLocationsWithPagination(this.sort.direction, this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.resultsLength = data.totalElements;
          return data.content;
        }),
        catchError(error => {
          this.errorHandlerService.handleError(error);
          this.isLoadingResults = false;
          return observableOf([]);
        })
      ).subscribe(data => this.data = data);
  }
 
  updateLocation(row) {
    this.dialogConfig.data = row; //send the entire data content to modal
    this.dialog.open(LocationModalComponent, this.dialogConfig);
  }
//create a new location
  createLocationDialog(){
    this.initializeModal();
    this.dialog.open(LocationModalComponent, this.dialogConfig);
  }
}
