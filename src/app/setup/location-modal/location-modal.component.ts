import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SuccessDialogComponent} from '../../shared/dialogs/success-dialog.component';
import {LocationService} from '../../service/location.service';
import {Location} from '../../shared/models';

@Component({
  templateUrl: './location-modal.component.html',
  styleUrls: ['./../setup.component.scss']
})
export class LocationModalComponent implements OnInit {

  locationForm: FormGroup;
  title: string = '';
  locationId: number;
  displayName: string;
  address: string;
  private dialogConfig;
  locationList: Location[] = [];

  constructor(private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<LocationModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialog: MatDialog,
    private locationService: LocationService) {

  }
  ngOnInit() {
    this.locationId = 0;
    this.dialogConfig = {
      disableClose: false,
      autoFocus: true,
      width: '450px'
    };

    if (this.data === null) {
      this.displayName = '';
      this.address = '';
      this.title = 'New Location';


    } else {
      this.displayName = this.data.displayName;
      this.address = this.data.address;
      this.title = 'Edit Location';
      this.locationId = this.data.id;
    }
    this.locationForm = this.formBuilder.group({
      name: [this.displayName, Validators.required],
      address: [this.address, Validators.required]
    });
  }

  get f() { return this.locationForm.controls; }

  save() {
    if (this.locationId > 0) {
      this.locationService.updateLocation(this.f.name.value, this.f.address.value, this.locationId)
        .subscribe((result:any) => {
          if (result.length !== 0) {
            this.dialogRef.close(this.locationForm.value);
            this.dialog.open(SuccessDialogComponent, this.dialogConfig);            
          }
        })
    } else {
      this.locationService.createLocation(this.f.name.value, this.f.address.value)
        .subscribe((result:any) => {
          if (result.length !== 0) {
            this.dialogRef.close(this.locationForm.value);
            this.dialog.open(SuccessDialogComponent, this.dialogConfig);
          }
        });
    }
    location.reload();
  }

  close() {
    this.dialogRef.close();
  }

}


