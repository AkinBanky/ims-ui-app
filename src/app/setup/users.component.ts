import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../service/user.service';
import {User} from '../shared/models';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {ErrorHandlerService} from '../service/error-handler.service';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {merge, of as observableOf} from 'rxjs';
import {UserModalComponent} from './user-modal/user-modal.component';

@Component({
  templateUrl: 'users.component.html',
  styleUrls: ['./setup.component.scss']

})
export class UsersComponent implements AfterViewInit, OnInit {
  displayedColumns: string[] = ['username', 'fullname', 'phone', 'status', 'role', 'update'];
  user: User[] = [];
  resultsLength = 0;
  isLoadingResults = true;
  private dialogConfig;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private userService: UserService,
    private dialog: MatDialog,
    private errorHandlerService: ErrorHandlerService) {}


  ngOnInit() {
    this.initializeModal();
  }

  private initializeModal() {
    this.dialogConfig = {
      disableClose: false,
      autoFocus: true,
      width: '450px'
    };
  }
  
  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.userService.retrievePaginatedUsers(this.sort.direction, this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.resultsLength = data.totalElements;
          return data.content;
        }),
        catchError(error => {
          this.errorHandlerService.handleError(error);
          this.isLoadingResults = false;
          return observableOf([]);
        })
      ).subscribe(data => this.user = data);
    }

  createUserDialog(){
    this.initializeModal();
    this.dialog.open(UserModalComponent, this.dialogConfig);
  }

  updateUser(row){
    this.dialogConfig.data = row;
    this.dialog.open(UserModalComponent, this.dialogConfig);
  }
 
}
