import {NgModule} from '@angular/core';

import {LocationsComponent} from './locations.component';
import {ProductsComponent} from './products.component';
import {
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import {CommonModule} from '@angular/common'; //imported by BrowserModule in root app module
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
// Forms Component
import {UsersComponent} from './users.component';
import {TablesComponent} from './tables.component';
// Modal Component
import {ModalModule} from 'ngx-bootstrap/modal';
import {LocationModalComponent} from './location-modal/location-modal.component';
// Components Routing
import {SetupRoutingModule} from './setup-routing.module';
import {UserModalComponent} from './user-modal/user-modal.component';
import {CategoryModalComponent} from './category-modal/category-modal.component';
import {CategoryComponent} from './category.component';
import {ProductModalComponent} from './product-modal/product-modal.component';
import {FileUploadModule} from "ng2-file-upload";
import {MatCardModule} from "@angular/material/card";
// import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
//import { UserComponent } from './user.component';

@NgModule({
  imports: [
    SetupRoutingModule,
    // BsDropdownModule.forRoot(),
    MatTableModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    CommonModule,
    MatSortModule,
    MatSelectModule,
    MatInputModule,
    FormsModule,
    MatIconModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    ModalModule.forRoot(),
    FileUploadModule,
    MatCardModule
  ],
  declarations: [
    LocationsComponent,
    ProductsComponent,
    UsersComponent,
    LocationModalComponent,
    UserModalComponent,
    TablesComponent,
    CategoryModalComponent,
    CategoryComponent,
    ProductModalComponent,
    //UserComponent,
  ],
  entryComponents :[LocationModalComponent, UserModalComponent,
    CategoryModalComponent, ProductModalComponent]
})
export class SetupModule { }
