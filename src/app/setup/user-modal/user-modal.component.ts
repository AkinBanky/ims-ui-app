import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {UserService} from '../../service/user.service';
import {SuccessDialogComponent} from '../../shared/dialogs/success-dialog.component';

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./../setup.component.scss']
})

export class UserModalComponent implements OnInit {
  userForm: FormGroup;
  title: string = '';
  action: string = '';

  name: string;
  email: string;
  fullname: string;
  mobile: string;
  roles: string;
  userId: number;

  private dialogConfig;
  constructor(private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<UserModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialog: MatDialog,
    private userService: UserService) { }

  ngOnInit() {
    this.dialogConfig = {
      disableClose: false,
      autoFocus: true,
      width: '450px'
    };

    if (this.data === null) {
      this.name = '';
      this.email = '';
      this.fullname = '';
      this.mobile = '';
      this.roles = '';
      this.title = 'New User';

    } else {
      this.name = this.data.displayName;
      this.email = this.data.emailAddress;
      this.fullname = this.data.fullName;
      this.mobile = this.data.mobileNumber;
      this.title = 'Edit User';
      this.userId = this.data.id;
      this.action = this.data.status === 'ACTIVATED'? DEACTIVATE : ACTIVATE;
      this.roles = this.data.role.userRoles;
    }
    this.userForm = this.formBuilder.group({
      name: [this.name, Validators.required],
      email: [this.email, Validators.required],
      fullname: [this.fullname, Validators.required],
      mobile: [this.mobile, Validators.required],
      roles: [this.roles,],
    });
  }
  get f() { return this.userForm.controls; }
  save() {
    let roles = this.f.roles.value + '';

    if(this.userId > 0) {
      //update User
      this.userService.updateUser(this.f.name.value, this.f.fullname.value,
        this.f.mobile.value, roles.split(','), this.f.email.value, this.userId)
        .subscribe((result: any) => {
          if (result.length !== 0) {
            this.dialogRef.close(this.userForm.value);
            this.dialog.open(SuccessDialogComponent, this.dialogConfig);
          }
        });
    } else {
      //create a new User
    this.userService.createUser(this.f.name.value, this.f.fullname.value,
      this.f.mobile.value, roles.split(','), this.f.email.value)
      .subscribe((result: any) => {
        if (result.length !== 0) {
          this.dialogRef.close(this.userForm.value);
          this.dialog.open(SuccessDialogComponent, this.dialogConfig);
        }
      });
    }
    this.dialogRef.close(this.userForm.value);
  }

  close() {
    this.dialogRef.close();
  }

  operation(event){
    let operation = event.target.textContent;
    var status;
    if(operation === DEACTIVATE){
        status = DEACTIVATE.concat('d');
    } else{
      status = ACTIVATE.concat('d');
    }

    this.userService.activateOrDeactivateStatus(status, this.data.id)
        .subscribe(res => {

        });
    this.dialogRef.close();
  }

}
const DEACTIVATE = "Deactivate";
const ACTIVATE = "Activate";
