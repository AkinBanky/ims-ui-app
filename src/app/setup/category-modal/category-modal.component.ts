import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material";
import {CategoryService} from "../../service/category.service";
import {SuccessDialogComponent} from "../../shared/dialogs/success-dialog.component";

@Component({
  selector: 'app-category-modal',
  templateUrl: './category-modal.component.html',
  styleUrls: ['./../setup.component.scss']
})
export class CategoryModalComponent implements OnInit {
  categoryForm: FormGroup;
  title: string = '';
  categoryId: number;
  displayName: string;
  private dialogConfig;
  constructor(private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<CategoryModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private dialog: MatDialog,
              private categoryService: CategoryService){ }

  ngOnInit() {
    this.categoryId = 0;
    this.dialogConfig = {
      disableClose: false,
      autoFocus: true,
      width: '250px'
    };

    if (this.data === null) {
      this.displayName = '';
      this.title = 'New Category';
    } else {
      this.displayName = this.data.displayName;
      this.title = 'Edit Category';
      this.categoryId = this.data.id;
    }
    this.categoryForm = this.formBuilder.group({
      name: [this.displayName, Validators.required]
    });
  }

  get f() { return this.categoryForm.controls; }

  save() {
    if (this.categoryId > 0) {
      this.categoryService.updateCategory(this.f.name.value, this.categoryId)
        .subscribe((result:any) => {
          if (result.length !== 0) {
            this.dialogRef.close(this.categoryForm.value);
            this.dialog.open(SuccessDialogComponent, this.dialogConfig);
          }
        })
    } else {
      this.categoryService.createCategory(this.f.name.value)
        .subscribe((result:any) => {
          if (result.length !== 0) {
            this.dialogRef.close(this.categoryForm.value);
            this.dialog.open(SuccessDialogComponent, this.dialogConfig);
          }
        });
    }
    location.reload();
  }
  close() {
    this.dialogRef.close();
  }



}
