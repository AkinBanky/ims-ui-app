import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {ProductService} from "../../service/product.service";
import {CategoryService} from "../../service/category.service";
import {Category, Location} from "../../shared/models";
import {LocationService} from "../../service/location.service";
import {FileUploader} from "ng2-file-upload";
import {SuccessDialogComponent} from "../../shared/dialogs/success-dialog.component";
import {ErrorHandlerService} from "../../service/error-handler.service";
import {HttpErrorResponse} from "@angular/common/http";
import {catchError} from "rxjs/operators";
import {throwError} from "rxjs";

@Component({
  selector: 'app-product-modal',
  templateUrl: './product-modal.component.html'
})

export class ProductModalComponent implements OnInit {
  productForm: FormGroup;
  public uploader: FileUploader = new FileUploader({
    isHTML5: true
  });
  title: string = '';
  uploadMessage: string = '';
  categoryList: Category[] = [];
  locationList: Location[] = [];
  show: boolean = false;
  name: string;
  category: string;
  location: string;
  size: number;
  images: any;
  whole: number;
  pieces: number;
  cost: number;
  productId: number;
  description: string;

  private dialogConfig;

  constructor(private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<ProductModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private dialog: MatDialog,
              private productService: ProductService,
              private categoryService: CategoryService,
              private locationService: LocationService,
              private errorHandlerService: ErrorHandlerService) {
  }

  ngOnInit() {
    this.categoryService.getAllCategories()
      .subscribe(res => {
        this.categoryList = res;
      });
    this.locationService.getAllLocations()
      .subscribe(res => {
        this.locationList = res;
      });

    this.dialogConfig = {
      disableClose: false,
      autoFocus: true,
      width: '450px'
    };
    if (this.data === null) {
      this.show = true;
      this.name = '';
      this.category = '';
      this.location = '';
      this.pieces = 0;
      this.size = 0;
      this.whole = 0;
      this.title = 'New Product';
      this.description = '';
      this.uploadMessage = 'Upload new';

    } else {
      this.show = false;
      this.category = this.data.category.displayName;
      this.location = this.data.location.displayName;
      this.name = this.data.displayName;
      this.size = this.data.cartonSize;
      this.whole = this.data.wholeQty;
      this.pieces = this.data.piecesQty;
      this.productId = this.data.id;
      this.cost = this.data.cartonCost;
      this.description = this.data.description;
      this.images = this.data.productImages;
      this.title = 'Edit Product';
      this.uploadMessage = 'Replace existing (optional)';
    }
    this.productForm = this.formBuilder.group({
      name: [this.name, Validators.required],
      size: [this.size, Validators.required],
      whole: [this.whole, Validators.required],
      pieces: [this.pieces, ''],
      description: [this.description, Validators.required],
      location: [this.location, Validators.required],
      category: [this.category, Validators.required],
      cost: [this.cost, Validators.required],
    });
  }

  get f() {
    return this.productForm.controls;
  }
  save() {

      const FILE_SIZE = 3000000;
      let data = new FormData();
      for (let j = 0; j < this.uploader.queue.length; j++) {
        let fileItem = this.uploader.queue[j]._file;
        if (fileItem.size > FILE_SIZE) {
          this.errorHandlerService.handle400Error("Each File should be less than 3MB in size.");
          let error = new HttpErrorResponse({
            status: 400,
            error: {
              errorMessage: "Each File should be less than 3MB in size."
            },
            statusText: "Each File should be less than 3MB in size."
          });
          this.errorHandlerService.handleError(error);
          return;
        }
        data.append('file', fileItem);
      }
    if(this.productId > 0) {
      this.productService.updateProduct(this.f.name.value, this.f.pieces.value, this.f.whole.value,
        this.f.cost.value, this.f.location.value, this.f.category.value,
        data, this.f.size.value, this.productId, this.f.description.value)
        .subscribe((result: any) => {
          if (result) {
            this.dialogRef.close(this.productForm.value);
            this.dialog.open(SuccessDialogComponent, this.dialogConfig);
          }
        });
    } else{
      this.productService.createProduct(this.f.name.value, this.f.pieces.value, this.f.whole.value,
        this.f.cost.value, this.f.location.value, this.f.category.value,
        data, this.f.size.value, this.f.description.value)
        .pipe(
          catchError(err => {
            this.errorHandlerService.handleError(err);
            return throwError(err);
          }
        ))

        .subscribe(
          res => {
            this.dialogRef.close(this.productForm.value);
            this.dialog.open(SuccessDialogComponent, this.dialogConfig)
          },
          err => console.log('Error Handled'),
          () => console.log('completed'));
    }
  }

  close() {
    this.dialogRef.close();
  }
}
