import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {LocationsComponent} from './locations.component';
import {ProductsComponent} from './products.component';
import {UsersComponent} from './users.component';
import {CategoryComponent} from "./category.component";
//import { UserComponent } from './user.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Setup'
    },
    children: [
      {
        path: 'location',
        component: LocationsComponent,
        data: {
          title: 'Location'
        }
      },
      {
        path: 'products',
        component: ProductsComponent,
        data: {
          title: 'Products'
        }
      },
      {
        path: 'users',
        component: UsersComponent,
        data: {
          title: 'Users'
        }
      },
      {
        path: 'category',
        component: CategoryComponent,
        data: {
          title: 'Category'
        }
      },
    /*   {
        path: 'userDialog',
        component: UserComponent,
        data: {
          title: 'User Dialog'
        }
      } */
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SetupRoutingModule {}
