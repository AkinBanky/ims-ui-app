import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Product} from "../shared/models";
import {MatDialog, MatPaginator, MatSort} from "@angular/material";
import {merge, of as observableOf} from "rxjs";
import {catchError, map, startWith, switchMap} from "rxjs/operators";
import {ErrorHandlerService} from "../service/error-handler.service";
import {ProductService} from "../service/product.service";
import {ProductModalComponent} from "./product-modal/product-modal.component";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  templateUrl: './products.component.html',
  styleUrls: ['./setup.component.scss']
})
export class ProductsComponent implements AfterViewInit, OnInit {
  productSearchForm: FormGroup;
  productName: string;
  displayedColumns: string[] = ['product', 'cost', 'size', 'whole','pieces', 'update'];
  data: Product[] = [];
  resultsLength = 0;
  isLoadingResults = true;
  private dialogConfig;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private dialog: MatDialog,
              private productService: ProductService,
              private formBuilder: FormBuilder,
              private errorHandlerService: ErrorHandlerService) { }

  private initializeModal() {
    this.dialogConfig = {
      disableClose: false,
      autoFocus: true,
      width: '550px'
    };
  }

  ngAfterViewInit(): void {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.productService.getAllProducts(this.sort.direction, this.sort.active, this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.resultsLength = data.totalElements;
          return data.content;
        }),
        catchError(error => {
          this.errorHandlerService.handleError(error);
          this.isLoadingResults = false;
          return observableOf([]);
        })
      ).subscribe(data => this.data = data);
  }

  ngOnInit(): void {
    this.initializeModal();
    this.productSearchForm = this.formBuilder.group({
      productName: [this.productName, Validators.required]
  });
  }

  createProductDialog(){
    this.initializeModal();
    this.dialog.open(ProductModalComponent, this.dialogConfig);
  }

  updateProduct(row){
    this.dialogConfig.data = row;
    this.dialog.open(ProductModalComponent, this.dialogConfig);
  }

  onSearch(event){
    let searchText = event.target.value;
    if(searchText === ''){
      this.errorHandlerService.handle400Error("Search text cannot be blank");
      return;
    }
    this.productService.getProductsBySearchText(searchText)
      .subscribe(res => {
        this.resultsLength = res.totalElements;
        this.data = res.content;
      });
  }
}
