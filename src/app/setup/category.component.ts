import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Category} from "../shared/models";
import {MatDialog, MatPaginator, MatSort} from "@angular/material";
import {ErrorHandlerService} from "../service/error-handler.service";
import {merge, of as observableOf} from "rxjs";
import {catchError, map, startWith, switchMap} from "rxjs/operators";
import {CategoryService} from "../service/category.service";
import {CategoryModalComponent} from "./category-modal/category-modal.component";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./setup.component.scss']
})
export class CategoryComponent implements AfterViewInit, OnInit {
  displayedColumns: string[] = ['category', 'update'];
  data: Category[] = [];
  resultsLength = 0;
  isLoadingResults = true;
  private dialogConfig;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private dialog: MatDialog,
              private categoryService: CategoryService,
              private errorHandlerService: ErrorHandlerService) { }

  ngOnInit() {
    this.initializeModal();
  }

  ngAfterViewInit(): void {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.categoryService.getAllCategoriesWithPagination(this.sort.direction, this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.resultsLength = data.totalElements;
          return data.content;
        }),
        catchError(error => {
          this.errorHandlerService.handleError(error);
          this.isLoadingResults = false;
          return observableOf([]);
        })
      ).subscribe(data => this.data = data);
  }

  createCategoryDialog(){
    this.initializeModal();
    this.dialog.open(CategoryModalComponent, this.dialogConfig);
  }

  updateCategory(row) {
    this.dialogConfig.data = row; //send the entire data content to modal
    this.dialog.open(CategoryModalComponent, this.dialogConfig);
  }

  private initializeModal() {
    this.dialogConfig = {
      disableClose: false,
      autoFocus: true,
      width: '250px'
    };
  }
}
