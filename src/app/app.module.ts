import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { MatDialogModule, MatTableModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown'; // Used for dropdowns

import { TabsModule } from 'ngx-bootstrap/tabs'; // Used for tabs
// see https://valor-software.com/ngx-bootstrap/#/tabs
import { NAV_DROPDOWN_DIRECTIVES } from './shared/nav-dropdown.directive';

import { ChartsModule } from 'ng2-charts/ng2-charts';
import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/sidebar.directive';
import { AsideToggleDirective } from './shared/aside.directive';
import { BreadcrumbsComponent } from './shared/breadcrumb.component';
import { AuthGuardComponent } from './authentication/authGuard.component';
import { CookieService } from 'ngx-cookie-service';

// Routing Module
import { AppRoutingModule } from './app.routing';
// Forms Module
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';
// import { MyUserCreateComponent } from './setup/my-user-create/my-user-create.component';

// Service
import { AuthenticationService } from './service/authentication.service';
import { UserService } from './service/user.service';
import { AlertService } from './service/alert.service';
import { AuthenticationModule } from './authentication/authentication.module';
import { LocationService } from './service/location.service';
import { SharedModule } from './shared/shared.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AddHeaderInterceptor } from './authentication/AddHeaderInterceptor';


@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    MatDialogModule,
    //MatTableModule,
    BrowserAnimationsModule,
    // HttpClientXsrfModule,
    AppRoutingModule,
    AuthenticationModule,
    SharedModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    AppComponent,
    FullLayoutComponent,
    SimpleLayoutComponent,
    NAV_DROPDOWN_DIRECTIVES,
    BreadcrumbsComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    AsideToggleDirective,

    //MyUserCreateComponent,
  ],
  // providers are creators of services that this module contributes to the global collection of services;
  // they become accessible in all parts of the app.
  entryComponents: [
    //MyUserCreateComponent
  ],
  providers: [
    AuthGuardComponent,
    AuthenticationService,
    LocationService,
    UserService,
    AlertService,
    CookieService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AddHeaderInterceptor,
      multi:true,
    },
    // CsrfService,
    {
    provide: LocationStrategy,
    useClass: HashLocationStrategy
    }
],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
